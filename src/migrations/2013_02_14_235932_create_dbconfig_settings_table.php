<?php

use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateDbconfigSettingsTable
 *
 * Schema
 */

class CreateDbconfigSettingsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
        /** @var \Illuminate\Database\Schema\Blueprint $table */
		Schema::create('dbconfig_settings', function($table)
		{

			$table->increments('id');
            $table->string('package', 100)->nullable();
            $table->string('group', 100)->default('config');
            $table->string('key', 100);
            $table->text('value')->nullable();
            $table->string('type', 25);
            $table->string('environment', 25)->nullable();
            $table->unique(array('package', 'key', 'environment'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dbconfig_settings');
	}

}